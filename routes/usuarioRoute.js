var express = require('express');
var usuario = require('../model/usuario');
var Autenticacion = require('../helper/autenticacion');
var router = express.Router();
var auth = new Autenticacion();

router.get('/autenticar', function(req, res) {
  res.render('default/autenticar', {title: 'Autenticar'});
});

router.get('/registrar', function(req, res) {
  res.render('default/registrar', {title: 'Registrar'});
});

router.get('/salir', function(req, res) {
  res.clearCookie('idUsuario');
  res.clearCookie('nick');
  res.redirect('/');
});

router.post('/autenticar', function(req, res) {
  var data =  {
    nick : req.body.nick,
    contrasena: req.body.contrasena
  }

  usuario.autenticar(data, function(err, resultado) {
    if(typeof resultado !== undefined) {
      res.cookie('nick', resultado[0].nick, { maxAge: 60 * 5000 });
      res.cookie('idUsuario', resultado[0].idUsuario, { maxAge: 60 * 5000 });
      res.redirect('/');
    } else {
      res.json({"Mensaje": "No existe usuario"});
    }
  });
});

router.post('/registrar', function(req, res) {
  var data =  {
    nick : req.body.nick,
    contrasena: req.body.contrasena
  }
  usuario.insert(data, function(err, resultado) {
    if(resultado) {
      usuario.autenticar(data, function(err, resultado) {
        if(typeof resultado !== undefined) {
          res.cookie('nick', resultado[0].nick, { maxAge: 60 * 5000 });
          res.cookie('idUsuario', resultado[0].idUsuario, { maxAge: 60 * 5000 });
          res.redirect('/');
        } else {
          res.json({"Mensaje": "No existe usuario"});
        }
      });
    } else {
      res.json({"Mensaje": "No se registro el usuario"});
    }
  });
});

router.get('/api/usuario/', function(req, res) {
  auth.autorizar(req);
  if(auth.getAcceso()) {
    usuario.select(auth.getIdUsuario(), function(error, resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No hay contactos"});
      }
    });
  } else {
    res.redirect('/autenticar');
  }
});

router.post('/actualizar', function(req, res) {
  auth.autorizar(req);
  var data =  {
    idUsuario : auth.getIdUsuario(),
    nick : req.body.nick,
    contrasena: req.body.contrasena
  }
  usuario.update(data, function(err, resultado) {
    if(resultado) {
      usuario.autenticar(data, function(err, resultado) {
        if(typeof resultado !== undefined) {
          res.cookie('nick', resultado[0].nick, { maxAge: 60 * 5000 });
          res.cookie('idUsuario', resultado[0].idUsuario, { maxAge: 60 * 5000 });
          res.redirect('/');
        } else {
          res.json({"Mensaje": "No existe usuario"});
        }
      });
    } else {
      res.json({"Mensaje": "No se registro el usuario"});
    }
  });
});

router.get('/eliminar', function(req, res) {
  auth.autorizar(req);
  if(auth.getAcceso()) {
    usuario.delete(auth.getIdUsuario(), function(error, resultados){
      if(typeof resultados !== undefined) {
        res.clearCookie('idUsuario');
        res.clearCookie('nick');
        res.redirect('/');
      } else {
        res.json({"Mensaje": "No hay contactos"});
      }
    });
  } else {
    res.redirect('/autenticar');
  }
});

module.exports = router;
