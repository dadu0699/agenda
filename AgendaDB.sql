CREATE DATABASE Agenda;
USE Agenda;

CREATE TABLE Categoria(
	idCategoria INT NOT NULL AUTO_INCREMENT,
    nombreCategoria VARCHAR(30) NOT NULL,
    PRIMARY KEY (idCategoria)
);

CREATE TABLE Contacto(
	idContacto INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    direccion VARCHAR(30) NOT NULL,
    telefono VARCHAR(12) NOT NULL,
    correo VARCHAR(40) NOT NULL,
    idCategoria INT NOT NULL,
    PRIMARY KEY (idContacto),
    FOREIGN KEY (idCategoria) REFERENCES Categoria(idCategoria)
);

CREATE TABLE Usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT,
    nick VARCHAR(30) NOT NULL,
    contrasena VARCHAR(30) NOT NULL,
    PRIMARY KEY (idUsuario)
);

CREATE TABLE UsuarioDetalle(
	idUsuarioDetalle INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idContacto INT NOT NULL,
    PRIMARY KEY (idUsuarioDetalle),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)ON DELETE CASCADE,
    FOREIGN KEY (idContacto) REFERENCES Contacto(idContacto)ON DELETE CASCADE
);

CREATE TABLE Historial(
	idHistorial INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    descripcion VARCHAR(255) NOT NULL,
    fecha DATE NOT NULL,
    PRIMARY KEY (idHistorial),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)ON DELETE CASCADE
);

-- VISTAS
DROP VIEW IF EXISTS usuario_contacto;
CREATE VIEW usuario_contacto AS 
	SELECT ud.idUsuario, c.idContacto, c.nombre, c.apellido, c.direccion, 
		c.telefono, c.correo, c.idCategoria, cat.nombreCategoria
	FROM UsuarioDetalle ud
	INNER JOIN Contacto c ON ud.idContacto = c.idContacto
    INNER JOIN Categoria cat ON c.idCategoria = cat.idCategoria;
