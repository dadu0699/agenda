var ContactoCliente = function() {
  var main = this;
  var contactoUri = "http://localhost:3000/api/contacto/";
  var categoriaUri = "http://localhost:3000/api/categoria/";
  var historialUri = "http://localhost:3000/api/historial/";
  var usuarioUri = "http://localhost:3000/api/usuario/";

  main.error = ko.observable();
  main.contactos = ko.observableArray([]);
  main.categorias = ko.observableArray([]);
  main.historiales = ko.observableArray([]);
  main.usuarios = ko.observableArray([]);

  main.contactoCargado = ko.observable();
  main.contactoNuevo = {
      nombre: ko.observable(),
      apellido: ko.observable(),
      direccion: ko.observable(),
      telefono: ko.observable(),
      correo: ko.observable(),
      Categoria: ko.observable()
  }
  main.categoriaNueva = {
      nombreCategoria: ko.observable()
  }

  function ajaxHelper(uri, method, data) {
    return $.ajax({
      url : uri,
      type: method,
      dataType: 'json',
      contentType: 'application/json',
      data: data ? JSON.stringify(data) : null
    }).fail(function(jqXHR, textStatus, errorThrown){
      console.log(errorThrown);
    })
  }

  main.getAllContactos = function() {
    ajaxHelper(contactoUri, 'GET').done(function(data) {
      main.contactos(data);
      limpiar();
    });
  }

  main.getAllCategorias = function () {
    ajaxHelper(categoriaUri, 'GET').done(function (data) {
      main.categorias(data);
      limpiar();
    });
  }

  main.getAllHistorial = function () {
    ajaxHelper(historialUri, 'GET').done(function (data) {
      main.historiales(data);
    });
  }

  main.getAllUsuario = function () {
    ajaxHelper(usuarioUri, 'GET').done(function (data) {
      main.usuarios(data);
    });
  }

  main.agregar = function () {
    var contacto = {
      nombre: main.contactoNuevo.nombre(),
      apellido: main.contactoNuevo.apellido(),
      direccion: main.contactoNuevo.direccion(),
      telefono: main.contactoNuevo.telefono(),
      correo: main.contactoNuevo.correo(),
      idCategoria: main.contactoNuevo.Categoria().idCategoria
    }
    ajaxHelper(contactoUri, 'POST', contacto).done(function (data) {
      main.getAllContactos();
    });
  }

  main.agregarCategoria = function () {
    var categoriaN = {
      nombreCategoria: main.categoriaNueva.nombreCategoria()
    }
    ajaxHelper(categoriaUri, 'POST', categoriaN).done(function (data) {
      main.getAllCategorias();
    });
  }

  main.editar = function () {
    var contacto = {
      idContacto: main.contactoCargado().idContacto,
      nombre: main.contactoCargado().nombre,
      apellido: main.contactoCargado().apellido,
      direccion: main.contactoCargado().direccion,
      telefono: main.contactoCargado().telefono,
      correo: main.contactoCargado().correo,
      idCategoria: main.contactoCargado().Categoria.idCategoria,
    }
    var uriEditar = contactoUri + contacto.ContactoId;
    ajaxHelper(uriEditar, 'PUT', contacto).done(function (data) {
      main.getAllContactos();
      $('#modalEditarContacto').modal('hide');
    });
  }

  main.eliminar = function (item) {
    var id = item.idContacto;
    var uri = contactoUri + id;
    ajaxHelper(uri, 'DELETE').done(function () {
      main.getAllContactos();
    });
  }

  main.cargar = function (item) {
    main.contactoCargado(item);
  }

  function limpiar() {
    main.contactoNuevo.nombre(null);
    main.contactoNuevo.apellido(null);
    main.contactoNuevo.direccion(null);
    main.contactoNuevo.telefono(null);
    main.contactoNuevo.correo(null);
    main.categoriaNueva.nombreCategoria(null);
    $('#modalAgregarContacto').modal('hide');
    $('#modalAgregarCategoria').modal('hide');
  }

  main.getAllContactos();
  main.getAllCategorias();
  main.getAllHistorial();
  main.getAllUsuario();
}

$(document).ready(function() {
  var contacto = new ContactoCliente();
  ko.applyBindings(contacto);
});
