-- PROCEDIMIENTOS ALMACENADOS
-- AGREGAR USUARIO
DROP procedure IF EXISTS `agregar_usuario`;
DELIMITER $$
CREATE PROCEDURE `agregar_usuario` (
	IN nombreUsuario VARCHAR(30),
	IN pass VARCHAR(30))
BEGIN
	INSERT INTO usuario(nick, contrasena) VALUES(nombreUsuario, pass);
END$$
DELIMITER ;

-- MODIFICAR USUARIO
DROP procedure IF EXISTS `modificar_usuario`;
DELIMITER $$
CREATE PROCEDURE `modificar_usuario`(
	IN id INT,
  IN nombreUsuario VARCHAR(30),
  IN pass VARCHAR(30))
BEGIN
	UPDATE usuario SET nick = nombreUsuario, contrasena = pass WHERE idUsuario = id;
END$$
DELIMITER ;

-- ELIMINAR USUARIO
DROP procedure IF EXISTS `eliminar_usuario`;
DELIMITER $$
CREATE PROCEDURE `eliminar_usuario`(
	IN id INT)
BEGIN
	DELETE FROM usuario WHERE idUsuario = id;
END$$
DELIMITER ;

-- AGREGAR CATEGORIA
DROP procedure IF EXISTS `agregar_categoria`;
DELIMITER $$
CREATE PROCEDURE `agregar_categoria` (IN nombreC VARCHAR(30))
BEGIN
	INSERT INTO categoria(nombreCategoria) VALUES(nombreC);
END$$
DELIMITER ;

-- AGREGAR CONTACTO
DROP procedure IF EXISTS `agregar_contacto`;
DELIMITER $$
CREATE PROCEDURE `agregar_contacto` (
	IN nombreN VARCHAR(30),
  IN apellidoN VARCHAR(30),
  IN direccionN VARCHAR(30),
  IN telefonoN VARCHAR(12),
  IN correoN VARCHAR(40),
  IN idCategoriaN INT)
BEGIN
	INSERT INTO contacto(nombre, apellido, direccion, telefono, correo, idCategoria)
		VALUES (nombreN, apellidoN, direccionN, telefonoN, correoN, idCategoriaN);
END$$
DELIMITER ;

-- MODIFICAR CONTACTO
DROP procedure IF EXISTS `modificar_contacto`;
DELIMITER $$
CREATE PROCEDURE `modificar_contacto`(
	IN id INT,
	IN nombreN VARCHAR(30),
	IN apellidoN VARCHAR(30),
  IN direccionN VARCHAR(30),
  IN telefonoN VARCHAR(12),
  IN correoN VARCHAR(40),
  IN idCategoriaN INT)
BEGIN
	UPDATE contacto SET nombre=nombreN, apellido=apellidoN, direccion=direccionN,
		telefono=telefonoN, correo=correoN, idCategoria=idCategoriaN
        WHERE idContacto=id;
END$$
DELIMITER ;

-- ELIMINAR CONTACTO
DROP procedure IF EXISTS `eliminar_contacto`;
DELIMITER $$
CREATE PROCEDURE `eliminar_contacto` (
	IN id INT)
BEGIN
	DELETE FROM contacto WHERE idContacto=id;
END$$
DELIMITER ;

-- AGREGAR USUARIODETALLE
DROP procedure IF EXISTS `agregar_usuariodetalle`;
DELIMITER $$
CREATE PROCEDURE `agregar_usuariodetalle` (
	IN idUsuarioN INT,
  IN idContactoN INT)
BEGIN
	INSERT INTO usuariodetalle (idUsuario, idContacto) VALUES (idUsuarioN, idContactoN);
END$$
DELIMITER ;

-- ELIMINAR USUARIODETALLE
DROP procedure IF EXISTS `eliminar_usuariodetalle`;
DELIMITER $$
CREATE PROCEDURE `eliminar_usuariodetalle` (
	IN idUsuarioN INT,
  IN idContactoN INT)
BEGIN
	DELETE FROM usuarioDetalle WHERE idUsuario=idUsuarioN AND idContacto=idContactoN;
END$$
DELIMITER ;

-- TRIGGERS
-- MODIFICAR USUARIO AL HISTORIAL
DROP trigger IF EXISTS tg_modificarUsuario;
DELIMITER $$
CREATE TRIGGER tg_modificarUsuario
	BEFORE UPDATE ON usuario
    FOR EACH ROW
BEGIN
    INSERT INTO historial(idUsuario, descripcion, fecha)
		VALUES (OLD.idUsuario, 'Se modifico el usuario y/o contraseña', now());
END$$
DELIMITER ;

-- AGREGAR CONTACTO AL HISTORIAL
DROP trigger IF EXISTS tg_agregarContacto;
DELIMITER $$
CREATE TRIGGER tg_agregarContacto
	AFTER INSERT ON usuariodetalle
	FOR EACH ROW
BEGIN

    SET @Datos = (SELECT CONCAT(nombre, ' ', apellido)
		FROM contacto WHERE idContacto = NEW.idContacto);

    INSERT INTO historial(idUsuario, descripcion, fecha)
		VALUES (NEW.idUsuario, CONCAT('Se agrego el contacto: ',  @Datos),
			now());
END$$
DELIMITER ;

-- ELIMINAR CONTACTO Al HISTORIAL
DROP trigger IF EXISTS tg_eliminarContacto;
DELIMITER $$
CREATE TRIGGER tg_eliminarContacto
	BEFORE DELETE ON contacto
    FOR EACH ROW
BEGIN
	SET @Datos = CONCAT(OLD.nombre, ' ', OLD.apellido);

    SET @idUsuario = (SELECT idUsuario FROM usuariodetalle
		WHERE idContacto = OLD.idContacto);

    INSERT INTO historial(idUsuario, descripcion, fecha)
		VALUES (@idUsuario, CONCAT('Se elimino el contacto: ',  @Datos),
			now());
END$$
DELIMITER ;
