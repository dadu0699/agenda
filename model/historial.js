var database = require('./database');
var historial = {};

historial.selectAll = function(idUsuario, callback) {
  if(database) {
    database.query("SELECT * FROM historial WHERE idUsuario = ?",
    idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = historial;
