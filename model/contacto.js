var database = require('./database');
var contacto = {};

contacto.selectAll = function(idUsuario, callback) {
  if(database) {
    database.query("SELECT * FROM usuario_contacto WHERE idUsuario = ?",
    idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

contacto.select = function(idContacto, callback) {
  if(database) {
    var sql = "SELECT * FROM Contacto WHERE idContacto = ?";
    database.query(sql, idContacto,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

contacto.insert = function(data, callback) {
  if(database) {
    var sql = "CALL agregar_contacto(?, ?, ?, ?, ?, ?)";
    database.query(sql,
    [data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {

        var sqlS = "CALL agregar_usuariodetalle(?, (SELECT idContacto FROM contacto WHERE nombre = ? AND apellido = ? " +
          "ORDER BY idContacto DESC LIMIT 1 ))";
        database.query(sqlS, [data.idUsuario, data.nombre, data.apellido],
        function(error, resultado1) {
          if(error) {
            throw error;
          } else {
            callback(null, resultado1);
          }
        });

      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

contacto.update = function(data, callback) {
  if(database) {
    var sql = "CALL modificar_contacto(?, ?, ?, ?, ?, ?, ?) ";
    database.query(sql,
    [data.idContacto, data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, data);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

contacto.delete = function(idContacto, callback) {
  if(database) {
    var sql = "CALL eliminar_contacto(?)";
    database.query(sql, idContacto,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = contacto;
